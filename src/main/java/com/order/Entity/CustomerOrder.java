package com.order.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



@Entity
@Table
public class CustomerOrder {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long orderId;
	@Column
	@JsonProperty("item-id")
	private String itemId;
	@Column
	@JsonProperty("item-type")
	private String itemType;
	@Column
	@JsonProperty("item-name")
	private String itemName;
	@Column
	@JsonProperty("item-quantity")
	private Float itemQuantity;
	@Column
	@JsonProperty("item-price")
	private Double itemPrice;
	@Column
	@JsonProperty("item-discount")
	private Double itemDiscount;
	@Column
	@JsonProperty("item-total-amount")
	private Double itemTotalAmount;
	
	@JsonIgnore
	private int weekendDiscount;
	@JsonIgnore
	private int typeDiscount;
	@JsonIgnore
	private String weekend;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Float getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(Float itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public Double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(Double itemPrice) {
		this.itemPrice = itemPrice;
	}
	public Double getItemTotalAmount() {
		return itemTotalAmount;
	}
	public void setItemTotalAmount(Double itemTotalAmount) {
		this.itemTotalAmount = itemTotalAmount;
	}
	public Double getItemDiscount() {
		return itemDiscount;
	}
	public void setItemDiscount(Double itemDiscount) {
		this.itemDiscount = itemDiscount;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getWeekend() {
		return weekend;
	}
	public void setWeekend(String weekend) {
		this.weekend = weekend;
	}
	public int getWeekendDiscount() {
		return weekendDiscount;
	}
	public void setWeekendDiscount(int weekendDiscount) {
		this.weekendDiscount = weekendDiscount;
	}
	public int getTypeDiscount() {
		return typeDiscount;
	}
	public void setTypeDiscount(int typeDiscount) {
		this.typeDiscount = typeDiscount;
	}
	



}
