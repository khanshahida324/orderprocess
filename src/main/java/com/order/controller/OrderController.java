package com.order.controller;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.order.Entity.Customer;
import com.order.Entity.CustomerAddress;
import com.order.Entity.CustomerOrder;
import com.order.Entity.DeliveryDetail;

import com.order.service.CustomerService;

@RestController
@Scope("request")
public class OrderController {
	
	
	private List<CustomerOrder> customerOrders = new ArrayList<CustomerOrder>();
	private CustomerAddress customerAddress = new CustomerAddress();
	private DeliveryDetail deliveryDetail = new DeliveryDetail();
	private Customer customer = new Customer();
	private Boolean weekend = Boolean.FALSE;
	
	
	@Autowired
    private CustomerService customerService;
	
	
	
	@RequestMapping(value="/saveOrderDetails", method = RequestMethod.POST )
	 ResponseEntity<Customer> processOrderDetails(@RequestBody String requestdata) {
		processRequestData(requestdata);
		
		return new ResponseEntity<>(customer,HttpStatus.OK);
	}
	
	private void processRequestData(String data) {
		 Stream.of(data.split("\n")).collect(Collectors.toList()).forEach(processData);;
		createCustomer();
		
	}
	
	Consumer<String> processData = line ->
    {
    	if(line.startsWith("A")) {
			addCustomerDetails(line);
		}else if(line.startsWith("I")) {
			addItemDeatils(line);
		}else if(line.startsWith("D")) {
			addDeliveryDetails(line);
		}
    };
	
	private void addCustomerDetails(String customer) {
		DateTimeFormatter df = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		if(customer.endsWith("\\n")) {
			String[] customerString = (customer.substring(0, customer.length()-2)).split("\\|");
		this.customer.setAccountId(customerString[1].toString());
		this.customer.setOrderDate(LocalDate.parse(customerString[2].toString(),df));
		this.customer.setAccountType(customerString[3].toString());
		this.weekend =(saturday.or(sunday).test(this.customer.getOrderDate().getDayOfWeek().toString()));
		}else {
			String[] customerStringN = customer.split("\\|");
			this.customer.setAccountId(customerStringN[1].toString());
			this.customer.setOrderDate(LocalDate.parse(customerStringN[2].toString(),df));
			this.customer.setAccountType(customerStringN[3].toString());
			this.weekend =(saturday.or(sunday).test(this.customer.getOrderDate().getDayOfWeek().toString()));
		}
		
	}
	
	Predicate<String> saturday = (i) -> i.equals("SATURDAY"); 
	Predicate<String> sunday = (i) -> i.equals("SUNDAY"); 
	
	private void addItemDeatils(String item) {
		CustomerOrder customerOrder = new CustomerOrder();
		if(item.endsWith("\\n")) {
			String[] orderString = (item.substring(0, item.length()-2)).split("\\|");
			customerOrder.setItemId(orderString[1].toString());
			customerOrder.setItemType(orderString[2].toString());
			customerOrder.setItemName(orderString[3].toString());
			customerOrder.setItemQuantity(Float.valueOf(orderString[4].toString()));
			customerOrder.setItemPrice(Double.valueOf(orderString[5].toString()));
			customerOrder.setWeekend(this.weekend ? "YES" : "NO");
		}else {
			String[] orderStringN = item.split("\\|");
			customerOrder.setItemId(orderStringN[1].toString());
			customerOrder.setItemType(orderStringN[2].toString());
			customerOrder.setItemName(orderStringN[3].toString());
			customerOrder.setItemQuantity(Float.valueOf(orderStringN[4].toString()));
			customerOrder.setItemPrice(Double.valueOf(orderStringN[5].toString()));
			customerOrder.setWeekend(this.weekend ? "YES" :"NO");
		}
		this.customerOrders.add(customerOrder);
		
	}
	
	 
	  
	
	private void addDeliveryDetails(String address) {
		if(address.endsWith("\\n")) {
			String[] addressString = (address.substring(0, address.length()-2)).split("\\|");
		this.deliveryDetail.setDeliveryAddress(addressString[1].toString());
		this.deliveryDetail.setDeliveryCity(addressString[2].toString());
		this.deliveryDetail.setDeliveryState(addressString[3].toString());
		this.deliveryDetail.setPincode(addressString[4].toString());
		this.deliveryDetail.setMobileno(addressString[5].toString());
		
		this.customerAddress.setDeliveryAddress(addressString[1].toString());
		this.customerAddress.setCity(addressString[2].toString());
		this.customerAddress.setState(addressString[3].toString());
		this.customerAddress.setPincode(addressString[4].toString());
		this.customerAddress.setMobileno(addressString[5].toString());
		}else {
			String[] addressN = address.split("\\|");
			this.deliveryDetail.setDeliveryAddress(addressN[1].toString());
			this.deliveryDetail.setDeliveryCity(addressN[2].toString());
			this.deliveryDetail.setDeliveryState(addressN[3].toString());
			this.deliveryDetail.setPincode(addressN[4].toString());
			this.deliveryDetail.setMobileno(addressN[5].toString());
			
			this.customerAddress.setDeliveryAddress(addressN[1].toString());
			this.customerAddress.setCity(addressN[2].toString());
			this.customerAddress.setState(addressN[3].toString());
			this.customerAddress.setPincode(addressN[4].toString());
			this.customerAddress.setMobileno(addressN[5].toString());
		}
	}
	
	private void createCustomer() {
		this.customer.setOrders(this.customerOrders);
		this.customer.setCustomerAddress(this.customerAddress);
		this.customer.setDeliveryDetail(deliveryDetail);
		customerService.applyRulesForCustomer(customer);
	}
	


}
