package com.order.Entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
@Table
@JsonPropertyOrder({ "account-id", "customer-address", "orders", "delivery-detail", "total-amount" })
public class Customer {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private Long customerId;
	@Column
	@JsonProperty("account-id")
	private String accountId;
	
	@Column
	@JsonIgnore
	private String accountType;
	
	@Column
	@JsonIgnore
	private LocalDate orderDate;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customerId")
	@JsonProperty("customer-address")
	private CustomerAddress customerAddress;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "customerId")
	@JsonProperty("orders")
	private List<CustomerOrder> orders = new ArrayList<CustomerOrder>();
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customerId")
	@JsonProperty("delivery-detail")
	private DeliveryDetail deliveryDetail;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customerId")
	@JsonProperty("total-amount")
	private TotalAmount totalAmount;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public CustomerAddress getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(CustomerAddress customerAddress) {
		this.customerAddress = customerAddress;
	}

	
	 public List<CustomerOrder> getOrders() { return orders; }
	  
	 public void setOrders(List<CustomerOrder> orders) { this.orders = orders; }
	 
	public DeliveryDetail getDeliveryDetail() {
		return deliveryDetail;
	}

	public void setDeliveryDetail(DeliveryDetail deliveryDetail) {
		this.deliveryDetail = deliveryDetail;
	}

	public TotalAmount getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(TotalAmount totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

}
